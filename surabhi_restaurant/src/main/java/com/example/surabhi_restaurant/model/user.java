package com.example.surabhi_restaurant.model;

import javax.persistence.*;

@Entity
public class user {
	@Id
	private int user_id;
	private String user_Name;
	private String password;
	
	
	//generating getters and setters...
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_Name() {
		return user_Name;
	}
	public void setUser_Name(String user_Name) {
		this.user_Name = user_Name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	//Generating constructor with default fields...
	public user(int user_id, String user_Name, String password) {
		super();
		this.user_id = user_id;
		this.user_Name = user_Name;
		this.password = password;
	}
	
	
	
	
	
}
