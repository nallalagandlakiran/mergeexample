package com.example.surabhi_restaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurabhiRestaurantApplication {

	public static void main(String[] args) {
		SpringApplication.run(SurabhiRestaurantApplication.class, args);
	}

}
